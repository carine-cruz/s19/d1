console.log(`ES6 Updates`);

/*
	Exponent Operator
*/

//old method
const SQUARED = Math.pow(4,3);
console.log(SQUARED);

const SQ = 4 ** 3;
console.log(SQ);

/*
	Template Literals
*/
console.log(` Template Literals use backticks and dollar $ + curly brace {}`);

/*
	Array Destructuring

	Syntax:
		let [variable, variable, variable] = refArray
*/

//old
let name = [`Cardo`,`Dela Cruz`,`Dalisay`];
// console.log(name[0]);
// console.log(name[1]);
// console.log(name[2]);

//array destructuring
	 let [fName, mName, lName] = name;

	 console.log(fName);
	 console.log(mName);
	 console.log(lName);

	 const grades = [91.5, 82.8, 90.9, 85.2];

	 function herGrades(arrOfGrades){
	 	console.log(arrOfGrades);

	 	const [one, two, three, four] = arrOfGrades;
	 	//if three not declared
	 	//const [one, two, ,four]

	 	// console.log(one);
	 	// console.log(two);
	 	// console.log(four);

	 	return `Her grade in English is ${one}, in Math is ${two}, and in Science is ${four}.`
	 }

	 console.log(herGrades(grades));

/*
	Object Destructuring

		Syntax:

		let {property, property, property} = refObject;
*/

let flower = {
	flowerName: `Rose`,
	type: `thornless`,
	color: `red`
}

let {flowerName, type, color, season} = flower;

/*console.log(flowerName);
console.log(type);
console.log(color);
console.log(season); //returns undefined*/

function describeFlower(bulaklak){
	//console.log(bulaklak)

	let {flowerName, type, color} = bulaklak;

	console.log(flowerName);
	console.log(type);
	console.log(color);

	return `I have ${color}, ${type} ${flowerName}.`
}

console.log(describeFlower(flower));

/*
	Arrow Function
*/

//traditional
function introduce1(name){
	return `My name is ${name}.`
}

//arrow
const introduce2 = (name) => {
	return `My name is ${name}.`
}

console.log(introduce1(`Denise`));
console.log(introduce2(`Carine`));

/*grades.forEach(function(){

})*/

//grades.forEach((grade) => {console.log(grade)});

/*Explicit return of arrow function 
    more than 1 */
let mappedGrades = grades.map(grade=> {
	return grade
})
console.log(mappedGrades);

/*Implicit return of arrow function
  can omit return */
let mappedName = name.map(person => person)
console.log(mappedName);

//console.log(name);

//name.forEach(person => console.log(person));
	//curly brace removed
	//parenthesis removed if only 1 parameter


/*
	Default Function Argument Value
*/

const greet = (name=`guest`) => `Good morning, ${name}!`;

console.log(greet());
console.log(greet(`Carine`));

function greeting(name=`user`){
	return `Good morning, ${name}!`
}

console.log(greeting());
console.log(greeting(`Denise`));


/*
	Class Based Object Blueprints
		-allows to instantiate objects using blueprints
		-constructor

		Syntax:

		class className{
			constructor(propertyA, propertyB){
				this.objProperty = property;
				this.objProperty = property
			}
		}

*/
	class Car{
		constructor(name, model, make, color, type){
			this.carName = carName;
			this.model = model;
			this.make = make;
			this.color = color;
			this.type = type;
		}
	}

	let car1 = new Car(`Ford`, `Fiesta`, 2021, `red`, `sedan`);
	console.log(car1);
	console.log(car1.carName);